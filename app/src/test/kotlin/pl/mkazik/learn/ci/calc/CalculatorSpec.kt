package pl.mkazik.learn.ci.calc

import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.junit.jupiter.api.Assertions.assertEquals

class CalculatorSpec {
    @Test
    fun `when string is empty then exception InvalidExpression is thrown`() {
        val calculator = Calculator()
        val expectedMessage = "Input source doesn't contain any expression"

        val exception = assertThrows<InvalidExpression> { calculator.parse("") }
        assertEquals(expectedMessage, exception.message)
    }

    @Test
    fun `when string is blank then exception InvalidExpression is thrown`() {
        val calculator = Calculator()
        val expectedMessage = "Input source doesn't contain any expression"

        val exception = assertThrows<InvalidExpression> { calculator.parse("\t    \t\r\n ") }
        assertEquals(expectedMessage, exception.message)
    }

    @Test
    fun `when single operand is available then result is evaluate to that value`() {
        val calculator = Calculator()
        calculator.parse("3")
        assertEquals(3, calculator.evaluate())
    }

    @Test
    fun `when expression is addition then result is evaluate to sum`() {
        val calculator = Calculator()
        calculator.parse("3+2")
        assertEquals(5, calculator.evaluate())
    }

    @Test
    fun `when expression is multiplication then result is evaluate to multiply`() {
        val calculator = Calculator()
        calculator.parse("4*3")
        assertEquals(12, calculator.evaluate())
    }

    @Test
    fun `when expression is subtraction then result is evaluate to subtract`() {
        val calculator = Calculator()
        calculator.parse("2-6")
        assertEquals(-4, calculator.evaluate())
    }

    @Test
    fun `when expression is subtraction reversed then result is evaluate correctly`() {
        val calculator = Calculator()
        calculator.parse("6-2")
        assertEquals(4, calculator.evaluate())
    }

    @Test
    fun `when expression is integer division then result is evaluate correctly`() {
        val calculator = Calculator()
        calculator.parse("6/3")
        assertEquals(2, calculator.evaluate())
    }

    @Test
    fun `when expression is combined in subtraction with addition operators then evaluates with correct order`() {
        val calculator = Calculator()
        calculator.parse("6-2+1")
        assertEquals(5, calculator.evaluate())
    }

    @Test
    fun `when expression is multiplication combined with division then evaluates with correct order`() {
        val calculator = Calculator()
        calculator.parse("3+4/2")
        assertEquals(5, calculator.evaluate())
    }

    @Test
    fun `when expression is combined in multiple operators with parenthesis then evaluates parenthesis first`() {
        val calculator = Calculator()
        calculator.parse("6-(2+1)")
        assertEquals(3, calculator.evaluate())
    }
}