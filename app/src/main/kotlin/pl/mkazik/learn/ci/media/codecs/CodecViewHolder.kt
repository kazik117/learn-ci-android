package pl.mkazik.learn.ci.media.codecs

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import pl.mkazik.learn.ci.R

class CodecViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    val textView = itemView.findViewById<TextView>(R.id.text)
}
