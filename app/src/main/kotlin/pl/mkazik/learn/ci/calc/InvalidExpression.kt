package pl.mkazik.learn.ci.calc

import java.io.IOException

class InvalidExpression(message: String) : IOException(message)
