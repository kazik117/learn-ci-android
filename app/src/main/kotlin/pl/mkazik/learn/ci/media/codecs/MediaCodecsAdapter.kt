package pl.mkazik.learn.ci.media.codecs

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import pl.mkazik.learn.ci.R

class MediaCodecsAdapter : RecyclerView.Adapter<CodecViewHolder>() {
    private val data = mutableListOf<String>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CodecViewHolder {
        return CodecViewHolder(
            LayoutInflater
                .from(parent.context)
                .inflate(R.layout.item_simple, parent, false)
        )
    }

    override fun onBindViewHolder(holder: CodecViewHolder, position: Int) {
        holder.textView.text = data[position]
    }

    override fun getItemCount(): Int = data.size

    fun updateData(newList: List<String>) {
        data.clear()
        data.addAll(newList)
        notifyDataSetChanged()
    }
}
