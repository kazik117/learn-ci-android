package pl.mkazik.learn.ci.media

import android.content.res.Resources
import android.media.MediaCodec
import android.media.MediaExtractor
import android.media.MediaFormat
import android.os.ParcelFileDescriptor
import android.util.Log
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.Locale

class Unpacker(private val fd: ParcelFileDescriptor) {
    companion object {
        private const val TAG = "Unpacker"
    }

    init {
        Log.v(TAG, "Created processor for ${fd.fd}")
    }

    @Throws(IOException::class)
    fun decode(): String {
        val startTime = System.currentTimeMillis()
        val extractor = MediaExtractor()
        extractor.setDataSource(fd.fileDescriptor)
        val trackCount = extractor.trackCount

        var selectedTrack = -1
        var mime = ""
        for (index in 0 until trackCount) {
            val format = extractor.getTrackFormat(index)
            mime = format.getString(MediaFormat.KEY_MIME).orEmpty()
            if (mime.startsWith("audio/")) {
                selectedTrack = index
                break
            }
        }
        if (selectedTrack == -1) {
            throw Resources.NotFoundException("Can't find suitable audio format")
        }
        extractor.selectTrack(selectedTrack)

        val codec = MediaCodec.createDecoderByType(mime)
        codec.configure(extractor.getTrackFormat(selectedTrack), null, null, 0)
        codec.start()
        Log.v(
            TAG,
            "Selected [$selectedTrack] format ${extractor.getTrackFormat(selectedTrack)}" +
                " with codec ${codec.name} hw:${codec.codecInfo.isHardwareAcceleratedCompat()}"
        )

        // Synchronous mode
        var isFinished = false
        var isInputEnd = false

        while (!isFinished) {
            if (!isInputEnd) {
                val inputBufferIndex = codec.dequeueInputBuffer(1000)
                if (inputBufferIndex >= 0) {
                    val inputBuffer = codec.getInputBuffer(inputBufferIndex)!!
                    var size = extractor.readSampleData(inputBuffer, 0)
                    var presentationTimeUs = 0L
                    if (size > 0) {
                        Log.v(TAG, "Read $size bytes and pass to decoder")
                        presentationTimeUs = extractor.sampleTime
                    } else {
                        Log.v(TAG, "End of input file")
                        isInputEnd = true
                        size = 0
                    }
                    codec.queueInputBuffer(
                        inputBufferIndex,
                        0,
                        size,
                        presentationTimeUs,
                        if (isInputEnd) MediaCodec.BUFFER_FLAG_END_OF_STREAM else 0
                    )
                    if (!isInputEnd) {
                        extractor.advance()
                    }
                }
            }
            val bufferInfo = MediaCodec.BufferInfo()
            val outputBufferIndex = codec.dequeueOutputBuffer(bufferInfo, 1000)
            if (outputBufferIndex >= 0) {
                codec.getOutputBuffer(outputBufferIndex)!!
                Log.v(TAG, "Decoded ${bufferInfo.size} bytes ready to process it")
                codec.releaseOutputBuffer(outputBufferIndex, false)
                if (bufferInfo.flags and MediaCodec.BUFFER_FLAG_END_OF_STREAM != 0) {
                    isFinished = true
                }
            } else if (outputBufferIndex == MediaCodec.INFO_TRY_AGAIN_LATER) {
                Log.v(TAG, "Output buffer not available.. Skip")
            } else if (outputBufferIndex == MediaCodec.INFO_OUTPUT_FORMAT_CHANGED) {
                Log.v(TAG, "Output format changed ${codec.outputFormat}")
            } else {
                Log.v(TAG, "Output buffer returned $outputBufferIndex")
            }
        }

        codec.stop()
        codec.release()
        extractor.release()
        val processTime = System.currentTimeMillis() - startTime
        return "Done in time ${SimpleDateFormat("mm:ss.SSS", Locale.US).format(processTime)}"
    }
}
