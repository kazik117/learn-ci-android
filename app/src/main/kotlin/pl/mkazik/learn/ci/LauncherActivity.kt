package pl.mkazik.learn.ci

import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.TextView
import androidx.activity.result.contract.ActivityResultContracts
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import pl.mkazik.learn.ci.media.MediaUnpackerViewModel
import pl.mkazik.learn.ci.media.codecs.MediaCodecsAdapter

class LauncherActivity : AppCompatActivity() {
    private val model by viewModels<MediaUnpackerViewModel>()

    private val pickMediaFile =
        registerForActivityResult(ActivityResultContracts.OpenDocument()) { uri: Uri? ->
            Log.v("Activity", "Selected uri for process $uri")
            uri?.let {
                model.decodeMedia(it)
            }
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val responseView = findViewById<TextView>(R.id.response_view)
        model.result.observe(
            this,
            { formatted ->
                responseView.text = formatted
            }
        )
        findViewById<Button>(R.id.open_document).setOnClickListener {
            pickMediaFile.launch(arrayOf("audio/*"))
        }

        val recyclerView = findViewById<RecyclerView>(R.id.list)
        val adapter = MediaCodecsAdapter()
        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(this)

        model.availableCodecs.observe(
            this,
            { list ->
                adapter.updateData(list)
            }
        )
    }
}
