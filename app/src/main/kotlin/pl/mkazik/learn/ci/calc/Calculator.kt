package pl.mkazik.learn.ci.calc

import java.util.Stack

/**
 * Simple implementation of calculator based on parsing expressions into tree.
 * The tree then is evaluated and return results.
 * It supports only four operators +, -, * and /. And it works only for integer numbers from 0 to 9.
 */
class Calculator {
    private lateinit var stack: Stack<Node>

    /**
     * Parse the source string into expressions tree.
     *
     * @throws InvalidExpression when [source] is empty or blank (contain entirely whitespaces).
     */
    fun parse(source: String) {
        if (source.isEmpty() || source.isBlank()) {
            throw InvalidExpression("Input source doesn't contain any expression")
        }
        val infixStack = Stack<Char>()
        var postfix = ""
        for (char in source) {
            when {
                char.isDigit() -> postfix += char
                char == '(' -> infixStack.push(char)
                char == ')' -> {
                    while (infixStack.peek() != '(') {
                        postfix += infixStack.pop()
                    }
                    infixStack.pop()
                }
                else -> {
                    while (hasOperatorHigherPrecedence(infixStack, char)) {
                        postfix += infixStack.pop()
                    }
                    infixStack.push(char)
                }
            }
        }
        while (!infixStack.isEmpty()) {
            postfix += infixStack.pop()
        }

        stack = Stack<Node>()
        for (char in postfix) {
            if (char.isDigit()) {
                stack.push(Node(char))
            } else if (isOperator(char)) {
                // Stack reverses the order, so we need to reverse stack values
                val right = stack.pop()
                val left = stack.pop()
                stack.push(Node(char, left, right))
            }
        }
    }

    private fun hasOperatorHigherPrecedence(
        infixStack: Stack<Char>,
        char: Char
    ) = !infixStack.isEmpty() && precedence(char) <= precedence(infixStack.peek())

    private fun precedence(operator: Char): Int {
        return when (operator) {
            '*', '/' -> 2
            '+', '-' -> 1
            else -> -1
        }
    }

    /**
     * Evaluate the parsed expression.
     */
    fun evaluate(): Int {
        return postfixTreeOrderTraversal(stack.pop())
    }

    private fun postfixTreeOrderTraversal(node: Node?): Int {
        if (node != null) {
            if (!isOperator(node.char)) {
                return node.char.digitToInt()
            }
            val left = postfixTreeOrderTraversal(node.left)
            val right = postfixTreeOrderTraversal(node.right)
            return applyOperator(left, right, node.char)
        }
        return 0
    }

    private fun isOperator(char: Char): Boolean {
        return when (char) {
            '+', '-', '*', '/' -> true
            else -> false
        }
    }

    private fun applyOperator(left: Int, right: Int, operator: Char): Int {
        return when (operator) {
            '+' -> left + right
            '-' -> left - right
            '*' -> left * right
            '/' -> left / right
            else -> 0
        }
    }

    private data class Node(
        val char: Char,
        val left: Node? = null,
        val right: Node? = null
    )
}
