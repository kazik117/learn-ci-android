package pl.mkazik.learn.ci.media

import android.app.Application
import android.media.MediaCodecInfo
import android.media.MediaCodecList
import android.net.Uri
import android.os.Build
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.liveData
import androidx.lifecycle.switchMap
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class MediaUnpackerViewModel(app: Application) : AndroidViewModel(app) {
    private val contentResolver by lazy { getApplication<Application>().contentResolver }
    private val mutableLiveData = MutableLiveData<Uri>()

    fun decodeMedia(uri: Uri) {
        Log.v("ViewModel", "startDecode $uri")
        mutableLiveData.value = uri
    }

    val result: LiveData<String> = mutableLiveData
        .switchMap { uri ->
            liveData {
                val result = processMediaByUri(uri)
                emit(result)
            }
        }

    private suspend fun processMediaByUri(uri: Uri): String {
        @Suppress("BlockingMethodInNonBlockingContext")
        val fd = withContext(Dispatchers.IO) {
            contentResolver.openFileDescriptor(uri, "r")
        }
        return withContext(Dispatchers.Default) {
            fd?.use {
                @Suppress("BlockingMethodInNonBlockingContext")
                Unpacker(it).decode()
            }.orEmpty()
        }
    }

    val availableCodecs: LiveData<List<String>> = liveData {
        val allCodecs = listAllAvailableCodecs()
        emit(allCodecs)
    }

    private suspend fun listAllAvailableCodecs(): List<String> = withContext(Dispatchers.IO) {
        MediaCodecList(MediaCodecList.REGULAR_CODECS)
            .codecInfos
            .filter { codec -> !codec.isEncoder }
            .map { codec ->
                "${codec.name} -> hw:${codec.isHardwareAcceleratedCompat()}," +
                    " sw:${codec.isSoftwareOnlyCompat()}"
            }
    }
}

fun MediaCodecInfo.isHardwareAcceleratedCompat(): Boolean {
    return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
        isHardwareAccelerated
    } else {
        false
    }
}

fun MediaCodecInfo.isSoftwareOnlyCompat(): Boolean {
    return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
        isSoftwareOnly
    } else {
        false
    }
}
